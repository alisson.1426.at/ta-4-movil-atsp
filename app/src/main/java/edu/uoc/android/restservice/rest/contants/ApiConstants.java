package edu.uoc.android.restservice.rest.contants;

public class ApiConstants {
    /*
    DOCUMENTACION ECHA POR:
    BRUNO JAVIER CONSTANTE PALACIOS
    */


    // BASE URL
    public static final String BASE_GITHUB_URL = "https://api.github.com/";


    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "users/{owner}";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/users/{owner}/followers";
}
